<?php

/**
 * @file template.php
 */

drupal_add_js(array('themePath' => path_to_theme()), 'setting');

function newskin_theme() {
  return array(
    'user_login_block' => array(
    'template' => 'user-login',
    'arguments' => array('form' => NULL)
    )
  );
}