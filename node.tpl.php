<?php

/**
 * @file node.tpl.php
 */

?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky): ?> sticky<?php endif; ?>  <?php if (!$status): ?>node-unpublished<?php endif; ?>
<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>

  <div class="content clear-block">
    <?php print $content; ?>
  </div>
  <div class="clear-block">
   <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="content-terms terms"><?php print $terms; ?></div>
    <?php endif;?>
    </div>
    <?php if ($links): ?>
      <div class="content-links links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>
  <?php if ($view->name != "featured_content") { ?>
  <div class="h-rule">&nbsp;</div> 
  <?php 
  } 
  else { 
    echo "<br/>"; 
  } ?>
</div>

