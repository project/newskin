<?php

/**
 * @file user-login.tpl.php
 * Custom user-login block
 */

?>

<div id="login_left">
  <div class="form-item" id="edit-name-wrapper">
    <input type="text" maxlength="60" name="name" id="edit-name" size="15" value="" class="form-text required" />
  </div>
  <div class="form-item" id="edit-pass-wrapper">
    <input type="password" name="pass" id="edit-pass"  maxlength="60"  size="15"  class="form-text required" />
  </div>
  <div class="form-item" id="edit-openid-identifier-wrapper">
    <input type="text" maxlength="255" name="openid_identifier" id="edit-openid-identifier" size="13" value="" class="form-text" />
    <div class="description">
      <a href="http://openid.net/">What is OpenID?</a>
    </div>
  </div>
  <div class="form-item">
  <input type="submit" name="op" id="edit-submit" value="Log in"  class="form-submit" />
    <input type="hidden" name="form_id" id="edit-user-login-block" value="user_login_block"  />
    <input type="hidden" name="openid.return_to" id="edit-openid.return-to" value="http://news.openmap/openid/authenticate?destination=node"  />
  </div>
</div>
<div id="login_right">
  <form action="/node?destination=node"  accept-charset="UTF-8" method="post" id="user-login-form">
    <div class="item-list">
      <ul>
        <li class="openid-link">
          <a href="/%2523">Log in using OpenID</a>
        </li>
        <li class="user-link">
          <a href="/%2523">Cancel OpenID login</a>
        </li>
      </ul>
    </div>
    <div class="item-list">
      <ul>
        <li class="register first">
          <a href="/user/register" title="Create a new user account.">Create new account</a>
        </li>
        <li class="last forgot-pass">
          <a href="/user/password" title="Request new password via e-mail.">Request new password</a>
        </li>
      </ul>
    </div>
  </form>
</div>
