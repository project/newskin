$Id

@file - readme.txt


NEWSKIN Install - 

The theme uses jScrollpane, and other jquery plugins. Thus you will need to download them separately,
and put it into the scripts folder. The list of required files (and their availability locations)
are specified below - 

jQuery Corners (by David_Turnbull) -
Dowload from - http://plugins.jquery.com/files/jquery-corners-0.3.zip
Instructions - 
  - Copy the file jquery.corners.js (or the minified version) as 'corners.js' into the scripts folder.
  
jScrollpane (by Kevin Luck) - 
Option - 1 
  - Download / Install the drupal module jScrollpane from http://ftp.drupal.org/files/projects/jscrollpane-6.x-1.0.tar.gz

Option - 2 
  - Download the jScrollpane script from http://www.kelvinluck.com/assets/jquery/jScrollPane/scripts/jScrollPane.js

Instructions - 
  - Copy the file as 'scroll.js' to the scripts folder. (if you are using the jScrollpane-Drupal module, just enable it)
  - To make mouse-wheel scroll working (for jscrollpane), also get this file -
    http://www.kelvinluck.com/assets/jquery/jScrollPane/scripts/jquery.mousewheel.js and copy it as 'mousewheel.js' 
    into the scripts folder.