$(document).ready(function() {
	$('.rounded').corners('12px');
	$(window).resize(function() { $('#sidebar .block .content').jScrollPane({scrollbarWidth:8, scrollbarMargin:10, showArrows:false }); });
	$('#sidebar .block .content').jScrollPane({scrollbarWidth:8, scrollbarMargin:10, showArrows:false });
	$('.expand_btn').toggle(
		function () { $(this).find('img').attr('src', Drupal.settings.basePath + Drupal.settings.themePath + '/images/down.png'); $(this).prev().slideToggle('slow'); }, 
		function () { $(this).find('img').attr('src', Drupal.settings.basePath + Drupal.settings.themePath + '/images/up.png'); $(this).prev().slideToggle('slow');}
	);
});