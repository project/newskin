<?php

/**
 * @file search-block-form.tpl.php
 * Custom search box
 */
 
?>
<form accept-charset="UTF-8" method="post" id="search-block-form">
<div style="margin:0; padding:0; text-align:right">
 <input type="text" name="search_block_form" id="edit-search-block-form-1" size="20" value="Search.." onFocus="this.value = ''" onBlur="if(this.value == '') this.value = 'Search..'" title="Enter the terms you wish to search for." class="form-text" />
<input type="submit" name="op" id="edit-submit-search" value=""  class="form-submit" />
<?php print $search["hidden"]; ?>
</div></form>