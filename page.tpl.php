﻿<?php

/**
 * @file page.tpl.php
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print $head_title ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<?php print $scripts; ?>
	<meta http-equiv="Content-Style-Type" content="text/css" />
</head>
<body>
<div id="header_about"><div id="header_about_text"><?php print $header_about; ?></div></div>
<?php if ($user->uid) {?>
<div id="about_toggle" class="expand_btn"><img class="expand_image" src="<?php print base_path() . path_to_theme() .'/images/up.png'; ?>" alt="" /></div>
<?php } ?>
<!-- start headers -->
<div id="header_logo">
	<div id="logo_container"><a href="<?php print url(''); ?>"><?php print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />'; ?></a></div>
	<div id="login_zone"><?php print $login_zone; ?></div>
</div>
<div id="header_main">
	<div id="top_menu"><?php print $top_menu; ?></div>
	<div id="searchbox"><?php print $searchbox; ?></div>
</div>

<div id="header_tags"><?php print $header_tags; ?></div>
<?php if ($user->uid) {?>
<div id="tags_toggle" class="expand_btn"><img class="expand_image" src="<?php print base_path() . path_to_theme() .'/images/up.png'; ?>" alt="" /></div>
<?php } ?>
<!-- end headers -->
<div class="h-clear"></div>
<div class="h-clear"></div>
<!-- start columns -->
<div id="wrapper">
	<div id="column_content">
		<div id="column_content_wrapper">
	    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
        <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
        <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <?php print $help; ?>

		<?php if ($featured_content) { 
      print '<div id="featured_content"><div class="rounded">'. $featured_content .'</div></div>'; 
    } 
    if ($title): print '<h1'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h1>'; endif; 
    print $content; 
    ?>
		</div>
	</div>
	<div id="sidebar">
		<?php if ($sidebar_header): ?><div id="sidebar_header"><?php print $sidebar_header; ?></div><?php endif; ?>
		<div id="sidebar_left"><?php print $sidebar_left; ?></div>
		<div id="sidebar_right"><?php print $sidebar_right; ?></div>
	</div>
</div>
<!-- end columns -->
<div class="h-clear"></div>
<div id="footer"><a href="http://creativecommons.org/licenses/by-sa/3.0/nl/deed.en"><img src="http://northeastwestsouth.net/by.png" alt=" all content is licensed under a Creative Commons Share Alike license" style="border:0; width:190px;height:66px;" /></a></div>
<?php print $closure; ?>
</body>
</html>